import React from 'react';
import { DataTable, TableContainer, Table, TableHead, TableRow, TableHeader, TableBody, TableCell } from 'carbon-components-react';

const PageContent = () => {
  return (
    <div>
      <h1>Hari</h1>
      <TableContainer title="Test Table">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeader>Column 1</TableHeader>
              <TableHeader>Column 2</TableHeader>
              <TableHeader>Column 3</TableHeader>
              <TableHeader>Column 4</TableHeader>
              <TableHeader>Column 5</TableHeader>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>NewDevSept2023</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>NewDevSept2023</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>NewDevSept2023</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>NewDevSept2023</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>NewDevSept2023</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
              <TableCell>NewDevSept2023 2</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default PageContent;
