import React from "react";
import {
  Content,
  Header,
  HeaderGlobalBar,
  HeaderName,
  HeaderGlobalAction,
  SideNav,
  SideNavItems,
  SideNavLink,
} from "carbon-components-react";
import PageContent from "./PageContent";

const UIShell = () => {
  return (
    <>
      <Header aria-label="Carbon Header">
        <HeaderName href="#" prefix="">
          Hari App
        </HeaderName>
      </Header>
      <SideNav isFixedNav expanded isChildOfHeader={false}>
        <SideNavItems>
          <SideNavLink to="/dashboard">Dashboard</SideNavLink>
          <SideNavLink to="/profile">Profile</SideNavLink>
          <SideNavLink to="/settings">Settings</SideNavLink>
        </SideNavItems>
      </SideNav>
      <Content>
        {" "}
        <PageContent />
      </Content>
    </>
  );
};

export default UIShell;
