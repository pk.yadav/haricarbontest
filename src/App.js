import './App.css';
// import '../node_modules/carbon-components/css/carbon-components.css';
import '../node_modules/@carbon/styles/index.scss';
// import '../node_modules/carbon-components/scss/themes/dark.scss';
// import '../node_modules/carbon-components-react/carbon-components-react.css';

import UIShell from './UIShell';


function App() {
  return (
    <div className="App">
       <UIShell />
    </div>
  );
}

export default App;
